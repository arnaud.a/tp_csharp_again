﻿using System;

namespace CalculatriceConsole
{
    public class Program
    {
        public static int Multiplication(int facteur_gauche, int facteur_droit)
        {
            return facteur_gauche * facteur_droit;
        }
        public static int Addition(int facteur_gauche, int facteur_droit)
        {
            return facteur_gauche + facteur_droit;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        public static int Soustraction(int facteur_gauche, int facteur_droit)
        {
            return facteur_gauche - facteur_droit;
        }
        public static double Division(double facteur_gauche, double facteur_droit)
        {
            return facteur_gauche/facteur_droit;
        }
    }
}
