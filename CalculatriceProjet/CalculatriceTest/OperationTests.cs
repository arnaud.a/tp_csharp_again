using System;
using Xunit;

using CalculatriceConsole;

namespace CalculatriceTest
{
    public class OperationTest
    {
        [Fact]
        public void TestMultiplication()
        {
            Assert.Equal(4, Program.Multiplication(2,2));
            Assert.Equal(21, Program.Multiplication(3,7));
        }
        [Fact]
        public void TestAddition()
        {
            Assert.Equal(8, Program.Addition(4,4));
            Assert.Equal(16, Program.Addition(8,8));
        }
        [Fact]
        public void TestSoustraction()
        {
            Assert.Equal(3, Program.Soustraction(8,5));
        }
        [Fact]
        public void TestDivision()
        {
            Assert.Equal(4, Program.Division(8,2));
            Assert.Equal(2.5, Program.Division(5,2));
        }
    }
}
