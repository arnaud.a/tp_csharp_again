﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Saisissez votre annee de naissance");
            int anneeNaissance = int.Parse(Console.ReadLine());
            Console.WriteLine("Saisissez votre Nom");
            string Nom = (Console.ReadLine());
            Console.WriteLine("Saisissez Votre prenom");
            string Prenom =(Console.ReadLine());
            int actualYear = DateTime.Now.Year;
            int Age = actualYear - anneeNaissance;
            Console.WriteLine(String.Format("Votre prenom est {0}.",Prenom));
            Console.WriteLine(String.Format("Votre nom est {0}.",Nom));
            Console.WriteLine(String.Format("Vous avez {0} ans.", Age));
            if ((anneeNaissance % 4 == 0) && (anneeNaissance%100 != 0)||(anneeNaissance%400 == 0))
                Console.WriteLine("L'annee de votre naissance est bissextile");
            else
                Console.WriteLine("L'année de votre naissance n'est pas bissextile");
            Console.WriteLine("Jusqu'a combien dois-je compter?");
            int Compter = int.Parse(Console.ReadLine());
            for (int compteur = 0;compteur <= Compter; compteur++)
            {
                Console.WriteLine("{0} ", compteur);
            }
        }
    }
}
